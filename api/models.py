from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    email = models.CharField(verbose_name='email', max_length=255, unique=True)
    phone = models.CharField(max_length=255, unique=False)
    REQUIRED_FIELDS = ['username','first_name', 'phone', 'last_name']
    USERNAME_FIELD = 'email'

    class Meta:
        db_table = 'user'

    def get_username(self):
        return self


class BlogPost(models.Model):
    author_id = models.IntegerField()
    category = models.IntegerField()
    title = models.CharField(max_length=255)
    body = models.TextField()
    date_created = models.CharField(max_length=255)

    class Meta:
        db_table = "blog_post"


class BlogLike(models.Model):
    post_id = models.IntegerField()
    like = models.IntegerField()

    class Meta:
        db_table = "blog_like"


class BlogDisLike(models.Model):
    post_id = models.IntegerField()
    dislike = models.IntegerField()

    class Meta:
        db_table = "blog_dislike"


class BlogComment(models.Model):
    post_id = models.IntegerField()
    author_name = models.TextField()
    comment = models.TextField()
    comment_date = models.CharField(max_length=255)

    class Meta:
        db_table = "blog_comment"


class Category(models.Model):
    category = models.CharField(max_length=255)

    class Meta:
        db_table = "category"


