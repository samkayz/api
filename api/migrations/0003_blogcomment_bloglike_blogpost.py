# Generated by Django 3.0.3 on 2020-02-16 09:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_auto_20200216_1010'),
    ]

    operations = [
        migrations.CreateModel(
            name='BlogComment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('post_id', models.IntegerField()),
                ('comment', models.TextField()),
                ('comment_date', models.CharField(max_length=255)),
            ],
            options={
                'db_table': 'blog_comment',
            },
        ),
        migrations.CreateModel(
            name='BlogLike',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('post_id', models.IntegerField()),
                ('like', models.IntegerField()),
                ('dislike', models.IntegerField()),
            ],
            options={
                'db_table': 'blog_like',
            },
        ),
        migrations.CreateModel(
            name='BlogPost',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('author_id', models.IntegerField()),
                ('title', models.CharField(max_length=255)),
                ('body', models.TextField()),
                ('date_created', models.CharField(max_length=255)),
            ],
            options={
                'db_table': 'blog_post',
            },
        ),
    ]
