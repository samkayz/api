from django.shortcuts import render
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework import status
from rest_framework.response import Response
from django.db.models import Q
from .serializers import *
from datetime import datetime
import random
import string
import uuid
from .models import *


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def create_post(request):
    date = datetime.now().strftime("%d/%m/%Y %H:%M:%S %p")
    author_id = request.user.id
    category = request.data.get('category')
    title = request.data.get('title')
    body = request.data.get('body')

    c_post = {
        "author_id": author_id,
        "title": title,
        "body": body,
        "date_created": date,
        "category": category
    }
    serializer = BlogPostSerializers(data=c_post)
    if serializer.is_valid():
        serializer.save()

    p_id = BlogPost.objects.values('id').get(title=title)['id']

    p_action = {
        "post_id": p_id,
        "like": "0",
    }
    action = BlogLikeSerializers(data=p_action)
    if action.is_valid():
        action.save()

    d_action = {
        "post_id": p_id,
        "dislike": "0"
    }
    dislike = BlogDisLikeSerializers(data=d_action)
    if dislike.is_valid():
        dislike.save()
    success = {
        "status": "Blog Created Successfully"
    }

    return Response(data=success, status=status.HTTP_201_CREATED)


@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def like_post(request, id, *args, **kwargs):
    lk = BlogLike.objects.values('like').get(post_id=id)['like']
    new = (int(lk)) + 1
    print(new)
    try:
        b_like = BlogLike.objects.get(post_id=id)
    except BlogLike.DoesNotExist:
        return Response(status=status.HTTP_400_BAD_REQUEST)

    new_like = {
        "post_id": id,
        "like": new,
    }
    serializer = BlogLikeSerializers(b_like, data=new_like)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
    return Response(serializer.errors)


@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def dislike_post(request, id):
    dlk = BlogDisLike.objects.values('dislike').get(post_id=id)['dislike']
    new = (int(dlk)) + 1
    print(new)
    try:
        b_like = BlogDisLike.objects.get(post_id=id)
    except BlogDisLike.DoesNotExist:
        return Response(status=status.HTTP_400_BAD_REQUEST)

    new_dislike = {
        "post_id": id,
        "dislike": new,
    }
    serializer = BlogDisLikeSerializers(b_like, data=new_dislike)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
    return Response(serializer.errors)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def comment(request, id):
    c_user_first_name = request.user.first_name
    c_user_last_name = request.user.last_name
    date = datetime.now().strftime("%d/%m/%Y %H:%M:%S %p")

    com = request.data.get('comment')

    add_comment = {
        "post_id": id,
        "comment": com,
        "comment_date": date,
        "author_name": c_user_first_name + " " + c_user_last_name
    }
    serializer = BlogCommentSerializers(data=add_comment)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def create_category(request):
    category = request.data.get('category')

    cat = {
        "category": category
    }
    serializer = CategorySerializers(data=cat)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data)
    return Response(serializer.errors)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def all_post(request):
    try:
        snippet = BlogPost.objects.filter()
    except BlogPost.DoesNotExist:
        return Response(status=status.HTTP_400_BAD_REQUEST)

    serializer = BlogPostSerializers(instance=snippet, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def s_post(request, id):
    b_post = BlogPost.objects.all().get(id=id)
    b_d = BlogDisLike.objects.all().get(post_id=id)
    b_l = BlogLike.objects.all().get(post_id=id)
    c = BlogComment.objects.filter()
    com = []
    author = []
    date = []
    for i in c:
        com.append(i.comment)
        author.append(i.author_name)
        date.append(i.comment_date)
    da = {
        "blog_post": {
            "id": b_post.id,
            "author_id": b_post.author_id,
            "title": b_post.title,
            "body": b_post.body,
            "date_created": b_post.date_created,
            "category": b_post.category
        },
        "dislike": {
            "post_id": b_d.post_id,
            "dislike": b_d.dislike
        },
        "like": {
            "post_id": b_l.post_id,
            "like": b_l.like
        },
        "comment": {
            "comment": com,
            "author": author,
            "date": date
        }
    }
    return Response(data=da, status=status.HTTP_200_OK)
