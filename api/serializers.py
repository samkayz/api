from djoser.serializers import UserCreateSerializer, UserSerializer
from rest_framework import serializers
from .models import *


class UserCreateSerializer(UserCreateSerializer):
    class Meta(UserCreateSerializer.Meta):
        model = User
        fields = ('id', 'email', 'username', 'password', 'first_name', 'last_name', 'phone')


class BlogPostSerializers(serializers.ModelSerializer):
    class Meta:
        many = True
        model = BlogPost
        fields = "__all__"


class BlogCommentSerializers(serializers.ModelSerializer):
    class Meta:
        model = BlogComment
        fields = "__all__"


class BlogLikeSerializers(serializers.ModelSerializer):
    class Meta:
        model = BlogLike
        fields = '__all__'


class BlogDisLikeSerializers(serializers.ModelSerializer):
    class Meta:
        model = BlogDisLike
        fields = '__all__'


class CategorySerializers(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'

