from django.urls import path, include
from api import views


urlpatterns = [
    path('', include('djoser.urls')),
    path('', include('djoser.urls.authtoken')),
    path('post', views.create_post),
    path('like/<id>', views.like_post),
    path('dislike/<id>', views.dislike_post),
    path('comment/<id>', views.comment),
    path('category', views.create_category),
    path('all_post', views.all_post),
    path('s_post/<id>', views.s_post),
]
